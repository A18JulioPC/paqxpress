/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Pillado
 */
public class PqManager {
    @SuppressWarnings({"MismatchedQueryAndUpdateOfCollection", "FieldMayBeFinal"})
    private List<Cliente> cliente = new ArrayList<>();
    @SuppressWarnings("FieldMayBeFinal")
    private HashMap<Integer, Paquete> paquetes = new HashMap<>();
    
    public void altaCliente(String id, String nombre){
        boolean b = true;
        
        for (Cliente cliente1 : cliente) {
            if (cliente1.getId().equals(id)) {
                b = false;
                break;
            }
        }
        
        if (b) { this.cliente.add(new Cliente(id, nombre)); }
    }
    
    public List listaClientes(){
        return this.cliente;
    }
    
    public void altaEnvio(Cliente cliente, String destinatario, String destino){
        Paquete p = new Paquete(cliente, destinatario, destino);
        this.paquetes.put(p.getId(), p);
    }
    
    public String estadoPedido(int id){//id del paquete
        Paquete p = this.getPaquete(id);
        String info = p.informaEstado();
        
        return info;
    }
    
    public ArrayList<Paquete> listaReparto(){
        ArrayList<Paquete> info = new ArrayList<>();
        
        for (Map.Entry<Integer, Paquete> entrada: this.paquetes.entrySet()) {
            if (entrada.getValue().informaEstado().equals(new EnReparto().informaEstado())) {
                info.add(entrada.getValue());
            }
        }
        
        return info;
    }
    
    public ArrayList<Paquete> listaEnviosCliente(String id){//id del cliente
        ArrayList<Paquete> info = new ArrayList<>();
        
        for (Map.Entry<Integer, Paquete> entrada: this.paquetes.entrySet()) {
            if (entrada.getValue().getCliente().getId().equals(id)) {
                info.add(entrada.getValue());
            }
        }
        
        return info;
    }
    
    public Paquete getPaquete(int id) {
        return this.paquetes.get(id);
    }

    public HashMap<Integer, Paquete> getPaquetes() {
        return this.paquetes;
    }
}
