/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

/**
 *
 * @author Pillado
 */
public class Cliente {
    @SuppressWarnings("FieldMayBeFinal")
    private String id;
    @SuppressWarnings("FieldMayBeFinal")
    private String nombre;

    public Cliente(String id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }
    
    @Override
    public String toString(){
        return "[CIF/NIF: " + this.id + ", Nombre/Razón Social: " + this.nombre + "]";
    }
}
