/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.time.LocalDateTime;

/**
 *
 * @author Pillado
 */
public class Paquete {
    private final int MINID = 1000;
    private static int numId = 1;
    @SuppressWarnings("FieldMayBeFinal")
    private int id;
    @SuppressWarnings("FieldMayBeFinal")
    private LocalDateTime fAlta;
    @SuppressWarnings("FieldMayBeFinal")
    private Cliente cliente;
    @SuppressWarnings("FieldMayBeFinal")
    private String destinatario;
    @SuppressWarnings("FieldMayBeFinal")
    private String destino;
    private EstadoPq estado;

    @SuppressWarnings("static-access")
    public Paquete(Cliente cliente, String destinatario, String destino) {
        this.cliente = cliente;
        this.destinatario = destinatario;
        this.destino = destino;
        this.id = MINID + this.numId++;
        this.fAlta = LocalDateTime.now();
        this.estado = new Ordenado();
    }
    
    public void cambiaEstado(){
        this.estado.avanza(this);
    }
    
    public void setEstado(EstadoPq estado){
        this.estado = estado;
    }
    
    public String informaEstado(){
        return this.estado.informaEstado();
    }

    public int getId() {
        return this.id;
    }

    public String getfAlta() {
        return this.fAlta.toString();
    }

    public Cliente getCliente() {
        return this.cliente;
    }

    public String getDestinatario() {
        return this.destinatario;
    }

    public String getDestino() {
        return this.destino;
    }

    public EstadoPq getEstado() {
        return estado;
    }
}
