/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

/**
 *
 * @author Pillado
 */
public class Entregado implements EstadoPq  {

    @Override
    public void avanza(Paquete p) {
        throw new IllegalArgumentException("No se puede avanzar estado: Ultimo estado");
    }

    @Override
    public String informaEstado() {
        return "Envio entregado";
    }
    
}
