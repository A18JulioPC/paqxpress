/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

/**
 *
 * @author Pillado
 */
public class Ordenado implements EstadoPq{

    @Override
    public void avanza(Paquete p) {
        p.setEstado(new EnProceso());
    }

    @Override
    public String informaEstado() {
        return "Envio ordenado";
    }
    
}
