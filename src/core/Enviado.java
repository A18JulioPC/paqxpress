/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

/**
 *
 * @author Pillado
 */
public class Enviado implements EstadoPq {

    @Override
    public void avanza(Paquete p) {
        p.setEstado(new EnReparto());
    }

    @Override
    public String informaEstado() {
        return "Envio enviado";
    }
    
}
