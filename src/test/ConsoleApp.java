/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import core.Cliente;
import core.Paquete;
import core.PqManager;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Pillado
 */
public class ConsoleApp {

    /**
     * @param args the command line arguments
     */
    @SuppressWarnings("UnusedAssignment")
    public static void main(String[] args) {
        final String CL_STR = "\033[H\033[2J";
        Scanner sc = new Scanner(System.in);
        PqManager pq = new PqManager();
        char c = '#';
        boolean b = true;
        
        while (b) {
            repeat(c, 39);
            System.out.printf("%c%37s%c\n", c, " ", c);
            System.out.printf("%c%14spqExpress%14s%c\n", c, " ", " ", c);
            System.out.printf("%c --->--->--->--->--->--->--->--->--- %c\n", c, c);
            System.out.printf("%c%37s%c\n", c, " ", c);
            System.out.printf("%c [1] - Alta cliente%18s%c\n", c, " ", c);
            System.out.printf("%c [2] - Lista clientes%16s%c\n", c, " ", c);
            System.out.printf("%c [3] - Alta nuevo envío%14s%c\n", c, " ", c);
            System.out.printf("%c [4] - Consultar estado envío%8s%c\n", c, " ", c);
            System.out.printf("%c [5] - Cambiar estado envío%10s%c\n", c, " ", c);
            System.out.printf("%c [6] - Listar envíos por cliente%5s%c\n", c, " ", c);
            System.out.printf("%c [7] - Listar envíos en reparto%6s%c\n", c, " ", c);
            System.out.printf("%c [X] - Salir%25s%c\n", c, " ", c);
            System.out.printf("%c%37s%c\n", c, " ", c);
            repeat(c, 39);
            System.out.print("Opción > ");
            String op = sc.nextLine();
            op = (op.equals("x"))? "X" : op;
            System.out.println(CL_STR);
            
            switch (op) {
                case "1":
                    System.out.println("Alta de nuevo cliente >");
                    System.out.println();
                    System.out.print("CIF/NIF: ");
                    String id = sc.nextLine();
                    
                    System.out.print("Nombre/Razón Social: ");
                    String nombre = sc.nextLine();
                    
                    System.out.println("[C] Confirmar | [] Cancelar");
                    op = sc.nextLine();
                    op = (op.equals("c"))? "C" : op;
                    
                    if (op.equals("C")) {
                        pq.altaCliente(id, nombre);
                        
                        System.out.println(CL_STR);
                        
                        System.out.println("Cliente creado");
                        System.out.print("Presiona [RET] para continuar");
                        op = sc.nextLine();
                    }
                    
                    System.out.println(CL_STR);
                    
                    break;
                case "2":
                    System.out.println("Lista de clientes >");
                    System.out.println();
                    
                    List<Cliente> cl = pq.listaClientes();
                    
                    for (int i = 0; i < cl.size(); i++) {
                        System.out.println("CIF/NIF: " + cl.get(i).getId());
                        System.out.println("Nombre/Razón Social: " + cl.get(i).getNombre());
                    
                        System.out.println();
                        repeat('-', 40);
                        System.out.println();
                    }
                    
                    System.out.print("Presiona [RET] para continuar");
                    op = sc.nextLine();
                    
                    System.out.println(CL_STR);
                    
                    break;
                case "3":
                    System.out.println("Alta nuevo envío >");
                    System.out.println();
                    System.out.print("CIF/NIF: ");
                    String clID = sc.nextLine();
                    
                    cl = pq.listaClientes();
                    
                    for (Cliente cliente : cl) {
                        if (cliente.getId().equals(clID)) {
                            System.out.print("Destinatario: ");
                            String dest = sc.nextLine();
                            System.out.print("Destino: ");
                            String desti = sc.nextLine();
                            pq.altaEnvio(cliente, dest, desti);
                        }
                    }
                    
                    System.out.print("Presiona [RET] para continuar");
                    op = sc.nextLine();
                    
                    System.out.println(CL_STR);
                    
                    break;
                case "4":
                    System.out.println("Consultar estado envío >");
                    System.out.println();
                    System.out.print("ID de envío: ");
                    int idEnv = sc.nextInt();
                    sc.nextLine();
                    Paquete p = pq.getPaquete(idEnv);
                    
                    if (p != null) {
                        System.out.printf("Cliente:%15s%s\n", " ", p.getCliente());
                        System.out.printf("Fecha:%17s%s\n", " ", p.getfAlta());
                        System.out.printf("Destinatario:%10s%s\n", " ", p.getDestinatario());
                        System.out.printf("Destino:%15s%s\n", " ", p.getDestino());
                        System.out.printf("Situación:%13s%s\n", " ", p.informaEstado());
                    } else {
                        System.out.println("No se encontro ningun envío");
                    }
                    
                    System.out.print("Presiona [RET] para continuar");
                    op = sc.nextLine();
                    
                    System.out.println(CL_STR);
                    
                    break;
                case "5":
                    System.out.println("Cambiar estado envío >");
                    System.out.println();
                    System.out.print("ID de envío: ");
                    idEnv = sc.nextInt();
                    sc.nextLine();
                    
                    try {
                        p = pq.getPaquete(idEnv);
                        
                        if (p != null) {
                            p.cambiaEstado();
                            System.out.println("Se cambio el estado: " + p.informaEstado());
                        }
                    } catch (IllegalArgumentException e) {
                        System.out.println("Error: " + e.getMessage());
                    }
                    
                    System.out.print("Presiona [RET] para continuar");
                    op = sc.nextLine();
                    
                    System.out.println(CL_STR);
                    
                    break;
                case "6":
                    System.out.println("Listar envíos por cliente >");
                    System.out.println();
                    
                    System.out.print("CIF/NIF: ");
                    id = sc.nextLine();
                    
                    ArrayList<Paquete> paq = pq.listaEnviosCliente(id);
                    
                    for (Paquete paquete : paq) {
                        System.out.printf("Cliente:%15s%s\n", " ", paquete.getCliente());
                        System.out.printf("Fecha:%17s%s\n", " ", paquete.getfAlta());
                        System.out.printf("Destinatario:%10s%s\n", " ", paquete.getDestinatario());
                        System.out.printf("Destino:%15s%s\n", " ", paquete.getDestino());
                        System.out.printf("Situación:%13s%s\n", " ", paquete.informaEstado());
                        System.out.println();
                        repeat('-', 40);
                        System.out.println();
                    }
                    
                    System.out.print("Presiona [RET] para continuar");
                    op = sc.nextLine();
                    
                    System.out.println(CL_STR);
                    
                    break;
                case "7":
                    System.out.println("Listar envíos en reparto >");
                    System.out.println();
                    
                    paq = pq.listaReparto();
                    
                for (Paquete paquete : paq) {
                    System.out.printf("Cliente:%15s%s\n", " ", paquete.getCliente());
                    System.out.printf("Destino:%15s%s\n", " ", paquete.getDestino());
                    System.out.println();
                    repeat('-', 40);
                    System.out.println();
                }
                    
                    System.out.print("Presiona [RET] para continuar");
                    op = sc.nextLine();
                    
                    System.out.println(CL_STR);
                    
                    break;


                case "8":
                    //EASTER EGG :)
                    asciiArt();
                    break;
                case "X":
                    b = false;
                    break;
                default:
                    System.out.println("Opción incorrecta");
                    break;
            }
            
            if (!b) { break; }
        }
    }
    
    private static void repeat(char c, int n){
        for (int i = 0; i < n; i++) {
            System.out.print(c);
        }
        
        System.out.println();
    }
    
    private static void asciiArt(){
        try {
            FileReader fr = new FileReader("[8]_easterEgg.txt");
            
            int c = fr.read();
            
            while (c != -1) {                
                c = fr.read();
                char lt = (char) c;
                System.out.print(lt);
            }
        } catch (IOException ex) {
            System.out.println("No se ha encontrado el archivo: " + ex.getMessage());
        }
        
        System.out.println();
        System.out.println();
    }
}
